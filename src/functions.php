<?php

if (!function_exists('test')) {
    function test(string $description, Closure $fn): void
    {
        echo $description, "\n";
        $fn();
    }
}

if (!function_exists('dump')) {
    function dump(...$args)
    {
        foreach ($args as $arg) {
            var_dump($arg);
        }
    }
}

if (!function_exists('dumpe')) {
    function dumpe(...$args)
    {
        dump(...$args);
        die();
    }
}
